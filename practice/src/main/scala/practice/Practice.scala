package practice

import java.io.Closeable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.io.{BufferedSource, Source}

case class Bucket(min: Int, max: Int, count: Int)

case class Stats(lineCount: Int,
                 minLineLength: Int,
                 averageLineLength: Int,
                 maxLineLength: Int,
                 buckets: Seq[Bucket])


object Practice extends App {
  private val bucketSize = 10
  private val defaultFileName = "in.txt"

  private def getSource(fileName: String): BufferedSource =
    Source.fromResource(fileName)

  private def read(in: BufferedSource): Future[Iterator[String]] =
    Future(blocking(in.getLines()))

  private def printStats(stats: Stats): Unit = {
    import stats._
    println(
      s"""
         | Total line count: $lineCount
         | min: $minLineLength
         | avg: $averageLineLength
         | max: $maxLineLength
         |
         | buckets:
         |${
        buckets.map { b =>
          import b._
          s"   - $min-$max: $count"
        }.mkString("\n")
      }""".stripMargin)
  }

  /*
    Методы для разминки
   */

  def asyncWithResource[R <: Closeable, T](resource: R)(block: R => Future[T]): Future[T] =
    block(resource).andThen { case _ => resource.close() }

  def asyncCountLines: Future[Int] = {
    asyncWithResource((getSource(defaultFileName))){
      in => read(in).map(iterator => iterator.toSeq.size)
    }
  }

  def asyncLineLengths: Future[Seq[(String, Int)]] = {
//    asyncWithResource(getSource(defaultFileName))(read(_).map(_.toSeq)).map
//    { it => it.map(s => (s, s.length)) }
    asyncWithResource((getSource(defaultFileName))){
      in => read(in).map(iterator => iterator.map(s => (s, s.length)).toList)
    }
  }

  def asyncTotalLength: Future[Int] = {
    asyncWithResource((getSource(defaultFileName))){
      in => read(in).map(iterator => iterator.map(s => s.length).toList.foldLeft(0)(_ + _))
    }
  }
  def countShorterThan(maxLength: Int): Future[Int] = {
    asyncWithResource((getSource(defaultFileName))){
      in => read(in).map(iterator => iterator.map(s => s.length).toList.filter(_ < maxLength).size)
    }
}

  /*
    Sleep sort
    https://www.quora.com/What-is-sleep-sort
   */

  def printWithDelay(delay: FiniteDuration, s: String) = Future{
    Thread.sleep(delay.toMillis)
    println(s)
  }

  def sleepSort: Future[Unit] =
    {
      //asyncLineLengths.flatMap(seq => Future.sequence(seq.map(t => Future(printWithDelay(t._2.seconds, t._1)))).map(_ => ()))
      //Future.traverse(asyncLineLengths)(s => Future.sequence(s.map(t => printWithDelay(t._2.seconds, t._1)))).map(_ => ())
      val seq: Seq[(String, Int)] = Seq(("aa", 2), ("aaa", 3), ("aaaa", 4), ("aaaaa", 5))
      Future.traverse(seq)(t => printWithDelay(t._2.seconds, t._1)).map(_ => ())
    }

  /*
    Calculate file statistics
   */

  def splitToBuckets(linesWithLengths: Seq[(String, Int)]): Future[Seq[Bucket]] =
    ???

  def calculateStats: Future[Stats] =
    ???



  Await.result(sleepSort, 500.seconds)


}
